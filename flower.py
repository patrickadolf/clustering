import os, random, sys, math, numpy as np

class Flower:
    x1              = [1,1,0,2,3]
    x2              = [1,0,2,4,5]
    flower_data     = []
    centroid        = []
    old_cluster     = []
    new_centroid    = []
    cluster         = {}
    new_cluster     = {}
    is_converge     = False
    k               = 2
    iteration       = 0

    def define_centroid(self):
        for i in range(0, self.k):
            center = random.randint(0, len(self.flower_data) -1)
            if center not in self.centroid:
                self.centroid.append(self.flower_data[center])
            else:
                i=i-1

    def __init__(self):
        
        self.initialize_cluster()
        self.flower_data = self.prepare_list()
        while(not self.is_converge):
            if len(self.centroid) == 0:
                self.define_centroid()
                self.assign_to_cluster(self.centroid, self.cluster)
            else:
                self.iteration +=1
                isi = sum((j) for i in self.new_cluster for j in self.new_cluster[i])
                if isi == 0:
                    self.old_cluster = self.cluster
                else :
                    self.old_cluster = self.new_cluster
                self.calculate_new_centroid()
                self.assign_to_cluster(self.new_centroid, self.new_cluster)
                self.check_convergence(self.old_cluster, self.new_cluster)
            # if self.iteration == 100:
            #     break
        print self.new_cluster
        print self.old_cluster

    def initialize_cluster(self):
        list = []
        for i in range(0, self.k):
            list.append(i)
        self.cluster        = dict.fromkeys(list, [])
        self.new_cluster    = dict.fromkeys(list, [])

    def prepare_list(self):
        list_data = []
        with open('iris.data', 'r') as iris:
            for data in iris:
                if data.strip():
                    list_data.append(data.split(','))
        return list_data

    def assign_to_cluster(self, centroid, cluster):
        for i in range(0, len(self.flower_data)):
            clusterindex = self.eucledian_distance(self.flower_data[i], centroid)
            if (len(cluster[clusterindex]) == 0):
                cluster[clusterindex] = []
            cluster[clusterindex].insert(len(cluster[clusterindex]), i)
            # self.cluster[cluster] = list(map(int, self.cluster[cluster]))

    def check_convergence(self, old_cluster, new_cluster):
        if(cmp(old_cluster, new_cluster) == 0):
            self.is_converge = True
        else:
            self.new_cluster.clear()
            list = []
            for i in range(0, self.k):
                list.append(i)
            self.new_cluster    = dict.fromkeys(list, [])

    def eucledian_distance(self, sample_data, centroid):
        distance_list   = []
        for i in range(0, len(centroid)):
            try:
                distance_list.append(math.sqrt(
                            ((float(sample_data[0])-float(centroid[i][0])) ** 2) +
                            ((float(sample_data[1])-float(centroid[i][1])) ** 2) +
                            ((float(sample_data[2])-float(centroid[i][2])) ** 2) +
                            ((float(sample_data[3])-float(centroid[i][3])) ** 2)
                            ))
            except ValueError:
                pass
            except IndexError:
                pass
        try:
            return distance_list.index(min(distance_list))
        except ValueError:
            pass

    def calculate_new_centroid(self):
        if len(self.new_cluster[0]) > 0:
            cluster = self.new_cluster
        else:
            cluster = self.cluster
        data1 = []
        data2 = []
        data3 = []
        data4 = []
        for i in range(0, len(cluster)):
            for j in range(0, len(cluster[i])):
                # print self.flower_data[self.cluster[i][j]][0]
                data1.append(self.flower_data[cluster[i][j]][0])
                data2.append(self.flower_data[cluster[i][j]][1])
                data3.append(self.flower_data[cluster[i][j]][2])
                data4.append(self.flower_data[cluster[i][j]][3])
            data1 = list(map(float, data1))
            data2 = list(map(float, data2))
            data3 = list(map(float, data3))
            data4 = list(map(float, data1))
            new_centroid_cord = []
            new_centroid_cord.append(np.mean(data1))
            new_centroid_cord.append(np.mean(data2))
            new_centroid_cord.append(np.mean(data3))
            new_centroid_cord.append(np.mean(data4))
            self.new_centroid.append(new_centroid_cord)





kmean = Flower()
